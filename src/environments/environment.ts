// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAq6aiiA17mD4yZRNA-8MYUKRWKTE1cUBo",
    authDomain: "todos-60a7f.firebaseapp.com",
    databaseURL: "https://todos-60a7f.firebaseio.com",
    projectId: "todos-60a7f",
    storageBucket: "todos-60a7f.appspot.com",
    messagingSenderId: "456564846789"    
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
