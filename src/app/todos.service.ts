import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

delete(key)  {
  this.authService.user.subscribe(
    user =>{
      let uid = user.uid;
      this.db.list('user/'+uid +'/todos').remove(key);
    }
  )
}

update(key,text){
  this.authService.user.subscribe(
    user =>{
      let uid = user.uid;
      this.db.list('user/'+uid +'/todos').update(key,{'text':text});
    }
  )  
}

addTodo(text){
    let todo = {'text':text};
    this.authService.user.subscribe(
      user =>{
        let uid = user.uid;
        this.db.list('user/'+uid +'/todos').push(todo); 
      }
    )        
}


  constructor(private authService:AuthService, private db:AngularFireDatabase) { }
}
