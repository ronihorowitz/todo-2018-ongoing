import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {TodosService} from  '../todos.service';


@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output()
  sendToParent: EventEmitter<any> = new EventEmitter<any>();

  text;
  tempText;
  key;
  showButton = false;
  showEditField = false;

  showEdit(){
    this.showEditField = true;
    this.showButton = false;
    this.tempText = this.text; 
    console.log(this.showButton);
  }
  
  save(){
    this.showEditField = false;
    //call the service to save 
    this.todosService.update(this.key, this.text);
  } 
  
  cancel(){
    this.showEditField = false;
    this.text = this.tempText;
  }
  
  show(){
    if(this.showEditField) return;
    console.log('In show');
    this.showButton = true; 
  }

  hide(){
    this.showButton = false; 
  } 
  
  delete(){
    console.log(this.key)
    this.todosService.delete(this.key);
  }
  
  constructor(private todosService:TodosService) { }

  ngOnInit() {
   this.text = this.data.text;
   this.key = this.data.$key; 

  }

  send(){
    this.sendToParent.emit(this.text);
  }

}
