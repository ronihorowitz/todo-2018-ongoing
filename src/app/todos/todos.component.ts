import { Component, OnInit} from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import {TodosService} from  '../todos.service';
import {AuthService} from  '../auth.service';


@Component({
  selector: 'todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  
  todos = [];
  text;   
  
  catch($event){
    this.text = $event; 
  }
  
  addTodo(){
    this.todosService.addTodo(this.text);
    this.text = '';
  }

  constructor(private db: AngularFireDatabase, 
              private todosService:TodosService,
              private authService:AuthService) { }

  ngOnInit() { 
    this.authService.user.subscribe(user=>{
      if(!user) return;
      this.db.list('/user/' + user.uid + '/todos').snapshotChanges().subscribe(
        todos =>{
          this.todos = [];
          todos.forEach(
            todo => {
              let y = todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
            }
          )
        }
      )            
    });    
  }
}


