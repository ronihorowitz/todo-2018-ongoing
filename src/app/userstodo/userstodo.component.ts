import { Component, OnInit, OnChanges } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

@Component({
  selector: 'userstodo',
  templateUrl: './userstodo.component.html',
  styleUrls: ['./userstodo.component.css']
})
export class UserstodoComponent implements OnInit {

  todos = [];
  text; 
  user = 'jack';   
  
  catch($event){
    this.text = $event; 
  }  

  changeuser(){
    this.db.list('/users/'+this.user + '/todos').snapshotChanges().subscribe(
      todos =>{
        this.todos = [];
        todos.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y["$key"] = todo.key;
            this.todos.push(y);
          }
        )
      }
    )       
  }

  constructor(private db: AngularFireDatabase) { }


  ngOnInit() {
     this.db.list('/users/'+this.user + '/todos').snapshotChanges().subscribe(
      todos =>{
        this.todos = [];
        todos.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y["$key"] = todo.key;
            this.todos.push(y);
          }
        )
      }
    )    
  }
}
