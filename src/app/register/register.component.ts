import { Component, OnInit } from '@angular/core';
//import { AngularFireDatabase } from 'angularfire2/database';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email: string;
  password: string;
  name: string;
  error = '';

  constructor(private authService: AuthService, private router:Router) {}

  signup() {
    this.authService.signup(this.email, this.password)
      .then(value => {
        console.log('Success! ', value.user.uid);
        this.authService.updateProfile(value.user, this.name);
        this.authService.addUser(value.user,this.name);       
      })   
     .then(value=>{
        this.router.navigate(['/'])
      }).catch(err =>{
        this.error = err;
        console.log(err);
      })  
  }

  ngOnInit() {
  }

}